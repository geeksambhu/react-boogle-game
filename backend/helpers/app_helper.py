import random
import string

from .boogle_helper import make_trie, boogle_words
from .config  import dictionary_path

global LIST_OF_VALID_WORDS, DICTIONARY
DICTIONARY = make_trie(dictionary_path)

def get_random_string(stringLength=16):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))




def boogle_solver(string_list, dictionary=DICTIONARY):
    LIST_OF_VALID_WORDS = boogle_words(string_list, dictionary)
    return LIST_OF_VALID_WORDS
