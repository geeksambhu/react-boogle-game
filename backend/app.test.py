import unittest
import json
from app import app


class BasicTestCase(unittest.TestCase):
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get("/", content_type="html/text")
        self.assertEqual(response.status_code, 200)

    def test_template_render(self):
        html = ""
        response = app.test_client().get("/")
        with open("templates/index.html", "rb") as f:
            html = f.read()
        self.assertAlmostEqual(response.data, html)


class APITestCase(unittest.TestCase):
    def test_api_route(self):
        tester = app.test_client(self)
        response = tester.get("/api/v1/startgame")
        self.assertEqual(response.status_code, 200)

    def test_new_game_string(self):
        tester = app.test_client(self)
        response = tester.get("/api/v1/startgame")
        response = json.loads(response.data)
        self.assertEqual(response.get("status"), "success")
        self.assertEqual(len(response.get("gamestring")), 4)
        self.assertEqual(len(response.get("gamestring")[0]), 4)


if __name__ == "__main__":
    unittest.main()

