from flask import Flask, render_template, jsonify, session, flash, request
from helpers.app_helper import get_random_string, boogle_solver
from datetime import timezone, datetime, timedelta
import textwrap

app = Flask(
    __name__,
    template_folder="../frontend/build",
    static_folder="../frontend/build/static",
)
app.secret_key = "secret-yyuuy-99889-#3-676-ERER"
app.config["PERMANENT_SESSION_LIFETIME"] = timedelta(seconds=90)


@app.after_request
def add_headers(response):
    white_origin = ["http://localhost:3000"]
    if (
        "HTTP_ORIGIN" in request.environ
        and request.environ["HTTP_ORIGIN"] in white_origin
    ):
        response.headers.add("Content-Type", "application/json")
        response.headers.add("Access-Control-Allow-Origin", request.headers["Origin"])
        response.headers.add("Access-Control-Allow-Methods", "GET")
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add(
            "Access-Control-Allow-Headers", "Content-Type,Authorization"
        )
        response.headers.add(
            "Access-Control-Expose-Headers",
            "Content-Type,Content-Length,Authorization,X-Pagination",
        )
    return response


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/api/v1/startgame")
def start_game():
    matrix_size = 4
    game_string = get_random_string()
    response = dict()
    response["gamestring"] = get_random_string()
    response["time"] = int(datetime.now(tz=timezone.utc).timestamp() * 1000)
    response["status"] = "success"
    string_list = textwrap.wrap(response["gamestring"], matrix_size)
    response["gamestring"] = string_list
    session["word_list"] = boogle_solver(string_list)
    session["time"] = response.get("time")
    response["time"] += int(90000)
    session["user_list"] = []

    return jsonify(response)


@app.route("/api/v1/checkgame/<word>", methods=["GET"])
def checkgame(word):
    # check_time
    current_time = int(datetime.now(tz=timezone.utc).timestamp() * 1000)
    timedelta = (current_time - session.get("time", 0)) / 1000
    if timedelta > 90:
        # flash("Game Over")
        response = {"message": "Game Time out!!", "status": "aborted"}
        return jsonify(response)
    status = "failed"
    user_list = session.get("user_list")
    if word in session.get("word_list", []):
        status = "success"
        score = len(word)
        if word in user_list:
            score = 0
            status = "failed"
        user_list.append(word)
        session["user_list"] = user_list
        response = {"score": score, "status": "success", "word": word}
        return jsonify(response)
    else:
        response = {"message": "Word not found!!", "status": status, "word": word}
        return jsonify(response)


if __name__ == "__main__":
    app.run(debug=True)
