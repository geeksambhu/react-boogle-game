Boggle Game is a single player Word Puzzle game.

- Uses flask as backend
- Uses React as frontend and used Redux and redux-thunk for state management.

![Project image](assets/gameimage.png)

# Setup Instruction

Clone this repo and get started with the setup instruction

```bash
git clone https://gitlab.com/geeksambhu/react-boogle-game.git
```


## Backend setup

1. Create a virtual environment using python3
 
    ```python3 -m venv env ```

    1.1 Activate the python environment 
        

    ``` 
    source env/bin/activate
    ```


2. install python dependencies

    ```pip install -r backend/requirements.txt ```

## Frontend setup

1. Change directory to `frontend`

    ```cd frontend```
2. Install node modules

    ```yarn ``` or ```npm install```

3. Build the project
   
   ```yarn build ``` or ```npm run build```

## Run the game

1. cd to ``..`` 

    ```cd .. ```
2. Run the app

    ```python backend/app.py``` if you are in project's parent directory or ```python app.py``` if you are inside ``backend`` folder