const config = {
  development: {
    url: "http://localhost:5000",
  },
  production: {
    url: "",
  },
};

export default process.env.NODE_ENV === "development"
  ? config.development
  : config.production;
