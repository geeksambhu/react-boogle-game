import { combineReducers } from "redux";
import { reducer as notifications } from "react-notification-system-redux";
import gameReducer from "./gameReducer";

const rootReducer = combineReducers({
  game: gameReducer,
  notifications,
});

export default rootReducer;
