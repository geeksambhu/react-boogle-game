import {
  FETCH_WORDS,
  CHECK_WORDS,
  RESET_GAME,
  INPUT_ACTION,
} from "../actions/actions";

const initialState = {
  gamestring: ["", "", "", ""],
  timeinms: 0,
  totalScore: 0,
  scoreArray: {},
};
const GameReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_WORDS:
      return {
        gamestring: action.gamestring,
        timeinms: action.timeinms,
        scoreArray: {},
        totalScore: 0,
        inputvalue: "",
      };
    case CHECK_WORDS:
      return {
        ...state,
        scoreArray: action.scoreArray,
        totalScore: action.totalScore,
        inputvalue: "",
      };

    case RESET_GAME:
      return {
        ...state,
      };
    case INPUT_ACTION:
      return {
        ...state,
        inputvalue: action.inputvalue,
      };

    default:
      return state;
  }
};

export default GameReducer;
