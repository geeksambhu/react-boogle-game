import axios from "axios";
import { FETCH_WORDS } from "./actions";
import config from "../../config";

export const fetchWordsAction = () => {
  return (dispatch) => {
    axios
      .get(`${config.url}/api/v1/startgame`, { withCredentials: true })
      .then((res) => {
        const { data } = res;
        dispatch({
          type: FETCH_WORDS,
          gamestring: data.gamestring,
          timeinms: data.time,
          inputvalue: "",
          totalScore: 0,
          scoreArray: {},
        });
      });
  };
};

export default fetchWordsAction;
