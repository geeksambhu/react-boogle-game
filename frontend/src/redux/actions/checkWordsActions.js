import axios from "axios";
import { CHECK_WORDS } from "./actions";
import config from "../../config";

export const checkWords = (inputvalue, totalScore, scoreArray) => {
  return (dispatch) => {
    axios
      .get(`${config.url}/api/v1/checkgame/${inputvalue.toLowerCase()}`, {
        withCredentials: true,
      })
      .then((res) => {
        const { data } = res;
        const { word, score } = data;
        if (data.status === "success" && score > 0) {
          dispatch({
            type: CHECK_WORDS,
            totalScore: totalScore + parseInt(score),
            scoreArray: { ...scoreArray, [word]: score },
            inputvalue: "",
          });
        } else {
          dispatch({
            type: CHECK_WORDS,
            totalScore,
            scoreArray: { ...scoreArray },
            inputvalue: "",
          });
        }
      });
  };
};

export default checkWords;
