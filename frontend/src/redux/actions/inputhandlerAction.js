import { INPUT_ACTION } from "./actions";

export const inputHandlerAction = (value) => {
  return (dispatch) => {
    dispatch({
      type: INPUT_ACTION,
      inputvalue: value,
    });
  };
};

export default inputHandlerAction;
