export const FETCH_WORDS = "FETCH_WORDS";

export const CHECK_WORDS = "CHECK_WORDS";

export const RESET_GAME = "RESET_GAME";

export const INPUT_ACTION = "INPUT_ACTION";
