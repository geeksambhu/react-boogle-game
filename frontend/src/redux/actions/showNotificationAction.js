import { success, error, warning } from "react-notification-system-redux";

export const showNotification = (title, message) => (dispatch) => {
  const notificationOpts = {
    title,
    message,
    position: "tr",
    autoDismiss: 5,
  };

  const alertType = title.toLowerCase();
  let alert = null;

  switch (alertType) {
    case "success":
      alert = success(notificationOpts);
      break;
    case "error":
      alert = error(notificationOpts);
      break;

    case "info":
      alert = warning(notificationOpts);
      break;
    default:
      alert = success(notificationOpts);
  }
  dispatch(alert);
};

export default showNotification;
