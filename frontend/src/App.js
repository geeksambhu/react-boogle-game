import React from "react";
import { connect } from "react-redux";
import fetchWordAction from "./redux/actions/fetchWordAction";
import checkWordsAction from "./redux/actions/checkWordsActions";
import inputHandlerAction from "./redux/actions/inputhandlerAction";
import Board from "./components/Board/board.component";
import Score from "./components/Score/score.component";
import Input from "./components/Input/input.component";
import Timer from "./components/Timer/timer.component";

import "./App.scss";

class App extends React.Component {
  checkAnswers = () => {
    this.props.checkWordsAction(
      this.props.inputvalue,
      this.props.totalScore,
      this.props.scoreArray
    );
  };

  inputChangeHandler = (e) => {
    this.props.inputHandlerAction(e.target.value);
  };

  keyPress = (e) => {
    if (e.keyCode === 13) {
      this.onClickHandler(e);
    }
  };

  onClickHandler = (e) => {
    e.preventDefault();
    this.checkAnswers();
  };

  componentDidMount() {
    this.props.fetchWordAction();
  }
  render() {
    if (!this.props.timeinms) {
      return <h1>Loading</h1>;
    }
    return (
      <div className="grid-container">
        <div className="Board">
          <Board wordList={this.props.gamestring} />
          <div className="input">
            <Input
              onEnter={this.keyPress}
              value={this.props.inputvalue}
              placeholder="words"
              onChange={this.inputChangeHandler}
            />
          </div>
          <div className="submit">
            <button className="input-button" onClick={this.onClickHandler}>
              Submit Words
            </button>
          </div>
        </div>
        <div className="Score">
          <Score
            scoreArray={this.props.scoreArray}
            totalScore={this.props.totalScore}
          />
          <div className="Reset">
            <button
              className="input-button button-red"
              onClick={this.props.fetchWordAction}
            >
              Reset Game
            </button>
          </div>
        </div>
        <div className="Header">
          <div className="header-logo">Boogle Game</div>
          <div className="Timer">
            <Timer timeinms={this.props.timeinms} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gamestring: state.game.gamestring,
    timeinms: state.game.timeinms,
    scoreArray: state.game.scoreArray,
    totalScore: state.game.totalScore,
    inputvalue: state.game.inputvalue,
  };
};

export default connect(mapStateToProps, {
  fetchWordAction,
  checkWordsAction,
  inputHandlerAction,
})(App);