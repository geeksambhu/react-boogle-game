import React from "react";
import { render } from "@testing-library/react";
import Scoreboard from "./score.component";

test("renders Scoreboard component", () => {
  const { getByText } = render(
    <Scoreboard scoreArray={{ of: 2 }} totalScore={9} />
  );
  const linkElement = getByText(/Scoreboard/i);
  expect(linkElement).toBeInTheDocument();
});
