import React from "react";
import "./score.styles.scss";

const WordScoreList = ({ scoreArray, totalScore, props }) => {
  const words = Object.keys(scoreArray);

  const wordsList = words.map(function (word, index) {
    return <li key={index}>{word}</li>;
  });
  const scores = Object.values(scoreArray);
  const scoresList = scores.map(function (score, index) {
    return <li key={index}>{score}</li>;
  });

  return (
    <div className="scoreboard">
      <h2> Scoreboard</h2>
      <div className="word-list">
        <div className="words">
          <h2>WORD</h2>
          {wordsList}
        </div>
        <div className="scores">
          <h2>SCORE</h2>
          {scoresList}
        </div>
      </div>
      <div className="total-score">
        {/* TODO: Render label */}
        <h2>Total Score</h2>
        {/* TODO: Render totalScore */}
        <span>{totalScore}</span>
      </div>
    </div>
  );
};

export default WordScoreList;
