import React from "react";
import "./board.styles.scss";

const Board = ({ wordList, ...props }) => {
  const gridLetters = wordList.map((letters) => (
    <div class="words-row">
      {letters.split("").map((char) => (
        <div class="character">
          <span class="character-individual">{char}</span>
        </div>
      ))}
    </div>
  ));
  // console.log(gridLetters)
  return <div class="board">{gridLetters}</div>;
};

export default Board;
