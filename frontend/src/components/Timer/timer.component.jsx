import React, { useEffect, useState } from "react";
import "./timer.styles.scss";

const CountdownTimer = ({ timeinms }) => {
  const calculateTimeLeft = (timeinms) => {
    const now = new Date();
    const nowUTC = new Date(
      now.getUTCFullYear(),
      now.getUTCMonth(),
      now.getUTCDate(),
      now.getUTCHours(),
      now.getUTCMinutes(),
      now.getUTCSeconds()
    );
    const endLocal = new Date(timeinms);
    const endUTCDate = new Date(
      endLocal.getUTCFullYear(),
      endLocal.getUTCMonth(),
      endLocal.getUTCDate(),
      endLocal.getUTCHours(),
      endLocal.getUTCMinutes(),
      endLocal.getUTCSeconds()
    );
    const difference = endUTCDate - nowUTC;
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(timeinms));

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(calculateTimeLeft(timeinms));
    }, 1000);
  });

  const timerComponents = [];

  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return;
    }

    timerComponents.push(
      <span>
        {timeLeft[interval]} {interval}{" "}
      </span>
    );
  });

  return (
    <div>
      <h1> Timer</h1>
      {timerComponents.length ? (
        timerComponents
      ) : (
        <span className="span-class">Time's up!</span>
      )}
    </div>
  );
};

export default CountdownTimer;
