import React from "react";
import { render } from "@testing-library/react";
import Timer from "./timer.component";

test("renders Timer component", () => {
  const { getByText } = render(<Timer timeinms={9898989898} />);
  const linkElement = getByText(/Timer/i);
  expect(linkElement).toBeInTheDocument();
});
