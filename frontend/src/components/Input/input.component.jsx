import React from "react";
import "./input.styles.scss";

const Input = ({ value, onChange, onEnter }) => {
  return (
    <div className="input-wrapper">
      <input
        className="input-class"
        onKeyDown={onEnter}
        type="text"
        value={value}
        placeholder="words"
        onChange={onChange}
        required
      />
    </div>
  );
};

export default Input;
